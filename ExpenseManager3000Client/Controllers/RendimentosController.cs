﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using ExpenseManager3000Client.Models;
using ClienteMvcProdutos.Helpers;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;

namespace ExpenseManager3000Client.Controllers
{
    public class RendimentosController : Controller
    {

        // GET: Rendimentos
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Rendimentos");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var rendimento = JsonConvert.DeserializeObject<IEnumerable<Rendimento>>(content);
                return View(rendimento);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: Rendimentos/Create
        public async Task<ActionResult> Create()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoRendimentos/");
            string content = await response.Content.ReadAsStringAsync();
            var tiposRendimento = JsonConvert.DeserializeObject<IEnumerable<TipoRendimento>>(content);

            ViewBag.TipoRendimentoId = new SelectList(tiposRendimento, "TipoRendimentoId", "Descricao");
            return View();
        }

        // POST: Rendimentos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "RendimentoId,Valor,Descricao,TipoRendimentoId,Data,UserId")] Rendimento rendimento)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string rendimentoJSON = JsonConvert.SerializeObject(rendimento);
                HttpContent content = new StringContent(rendimentoJSON, System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/Rendimentos", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: Rendimentos/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Rendimentos/" + id);

            HttpResponseMessage response1 = await client.GetAsync("api/TipoRendimentos/");
            string content1 = await response1.Content.ReadAsStringAsync();
            var tiposRendimento = JsonConvert.DeserializeObject<IEnumerable<TipoRendimento>>(content1);

            ViewBag.TipoRendimentoId = new SelectList(tiposRendimento, "TipoRendimentoId", "Descricao");

            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var rendimentos = JsonConvert.DeserializeObject<Rendimento>(content);
                if (rendimentos == null) return HttpNotFound();
                return View(rendimentos);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: Rendimentos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "RendimentoId,Valor,Descricao,TipoRendimentoId,Data,UserId")] Rendimento rendimento)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string rendimentoJSON = JsonConvert.SerializeObject(rendimento);
                HttpContent content = new StringContent(rendimentoJSON, System.Text.Encoding.Unicode, "application/json");
                var response =
                await client.PutAsync("api/Rendimentos/" + rendimento.RendimentoId, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }

        }

        // GET: Rendimentos/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Rendimentos/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var rendimento = JsonConvert.DeserializeObject<Rendimento>(content);
                if (rendimento == null) return HttpNotFound();
                return View(rendimento);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: Rendimentos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/Rendimentos/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }

        }
    }
}
