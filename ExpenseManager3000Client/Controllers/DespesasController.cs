﻿using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using ExpenseManager3000Client.Models;
using ClienteMvcProdutos.Helpers;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ExpenseManager3000Client.Controllers
{
    public class DespesasController : Controller
    {

        // GET: Despesas
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Despesas");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var despesa = JsonConvert.DeserializeObject<IEnumerable<Despesa>>(content);
                return View(despesa);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: Despesas/Create
        public async Task<ActionResult> Create()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoDespesas/");
            HttpResponseMessage response1 = await client.GetAsync("api/MeioPagamentos/");
            string content = await response.Content.ReadAsStringAsync();
            string content1 = await response1.Content.ReadAsStringAsync();
            var tiposDespesa = JsonConvert.DeserializeObject<IEnumerable<TipoDespesa>>(content);
            var meiosPagamento = JsonConvert.DeserializeObject<IEnumerable<MeioPagamento>>(content1);

            ViewBag.TipoDespesaId = new SelectList(tiposDespesa, "TipoDespesaId", "Descricao");
            ViewBag.MeioPagamentoId = new SelectList(meiosPagamento, "MeioPagamentoId", "Descricao");
            return View();
        }

        // POST: Despesas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "DespesaId,Valor,Descricao,TipoDespesaId,MeioPagamentoId,Data,Comentarios,UserId")] Despesa despesa)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string despesaJSON = JsonConvert.SerializeObject(despesa);
                HttpContent content = new StringContent(despesaJSON, System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/Despesas", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: Despesas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Despesas/" + id);

            HttpResponseMessage response1 = await client.GetAsync("api/TipoDespesas/");
            HttpResponseMessage response2 = await client.GetAsync("api/MeioPagamentos/");
            string content1 = await response1.Content.ReadAsStringAsync();
            string content2 = await response2.Content.ReadAsStringAsync();
            var tiposDespesa = JsonConvert.DeserializeObject<IEnumerable<TipoDespesa>>(content1);
            var meiosPagamento = JsonConvert.DeserializeObject<IEnumerable<MeioPagamento>>(content2);

            ViewBag.TipoDespesaId = new SelectList(tiposDespesa, "TipoDespesaId", "Descricao");
            ViewBag.MeioPagamentoId = new SelectList(meiosPagamento, "MeioPagamentoId", "Descricao");

            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var despesas = JsonConvert.DeserializeObject<Despesa>(content);
                if (despesas == null) return HttpNotFound();
                return View(despesas);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: Despesas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "DespesaId,Valor,Descricao,TipoDespesaId,MeioPagamentoId,Data,Comentarios,UserId")] Despesa despesa)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string despesaJSON = JsonConvert.SerializeObject(despesa);
                HttpContent content = new StringContent(despesaJSON, System.Text.Encoding.Unicode, "application/json");
                var response =
                await client.PutAsync("api/Despesas/" + despesa.DespesaId, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: Despesas/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Despesas/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var despesa = JsonConvert.DeserializeObject<Despesa>(content);
                if (despesa == null) return HttpNotFound();
                return View(despesa);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: Despesas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/Despesas/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }
    }
}
