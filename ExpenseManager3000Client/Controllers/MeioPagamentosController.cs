﻿using System.Threading.Tasks;
using System.Web.Mvc;
using ExpenseManager3000Client.Models;
using ClienteMvcProdutos.Helpers;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;

namespace ExpenseManager3000Client.Controllers
{
    public class MeioPagamentosController : Controller
    {

        // GET: MeioPagamentos
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/MeioPagamentos");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var meiosPagamento = JsonConvert.DeserializeObject<IEnumerable<MeioPagamento>>(content);
                return View(meiosPagamento);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: MeioPagamentos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MeioPagamentos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "MeioPagamentoId,Descricao,UserId")] MeioPagamento meioPagamento)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string meiosPagamentoJSON = JsonConvert.SerializeObject(meioPagamento);
                HttpContent content = new StringContent(meiosPagamentoJSON, System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/MeioPagamentos", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: MeioPagamentos/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/MeioPagamentos/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var meiosPagamento = JsonConvert.DeserializeObject<MeioPagamento>(content);
                if (meiosPagamento == null) return HttpNotFound();
                return View(meiosPagamento);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: MeioPagamentos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "MeioPagamentoId,Descricao,UserId")] MeioPagamento meioPagamento)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string meiosPagamentoJSON = JsonConvert.SerializeObject(meioPagamento);
                HttpContent content = new StringContent(meiosPagamentoJSON, System.Text.Encoding.Unicode, "application/json");
                var response = await client.PutAsync("api/MeioPagamentos/" + meioPagamento.MeioPagamentoId, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: MeioPagamentos/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/MeioPagamentos/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var meiosPagamento = JsonConvert.DeserializeObject<MeioPagamento>(content);
                if (meiosPagamento == null) return HttpNotFound();
                return View(meiosPagamento);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: MeioPagamentos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/MeioPagamentos/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }
    }
}
