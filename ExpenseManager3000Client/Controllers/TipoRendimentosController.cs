﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using ExpenseManager3000Client.Models;
using ClienteMvcProdutos.Helpers;
using System.Net.Http;
using Newtonsoft.Json;

namespace ExpenseManager3000Client.Controllers
{
    public class TipoRendimentosController : Controller
    {

        // GET: TipoRendimentos
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoRendimentos");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var tiposRendimento = JsonConvert.DeserializeObject<IEnumerable<TipoRendimento>>(content);
                return View(tiposRendimento);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: TipoRendimentos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipoRendimentos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "TipoRendimentoId,Descricao,UserId")] TipoRendimento tipoRendimento)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string tipoRendimentoJSON = JsonConvert.SerializeObject(tipoRendimento);
                HttpContent content = new StringContent(tipoRendimentoJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/TipoRendimentos", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }

        }

        // GET: TipoRendimentos/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoRendimentos/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var tiposRendimento = JsonConvert.DeserializeObject<TipoRendimento>(content);
                if (tiposRendimento == null) return HttpNotFound();
                return View(tiposRendimento);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: TipoRendimentos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "TipoRendimentoId,Descricao,UserId")] TipoRendimento tipoRendimento)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var client = WebApiHttpClient.GetClient();
                    string tipoRendimentoJSON = JsonConvert.SerializeObject(tipoRendimento);
                    HttpContent content = new StringContent(tipoRendimentoJSON,
                    System.Text.Encoding.Unicode, "application/json");
                    var response =
                    await client.PutAsync("api/TipoRendimento/" + tipoRendimento.TipoRendimentoId, content);
                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return Content("Ocorreu um erro: " + response.StatusCode);
                    }
                }
                catch
                {
                    return Content("Ocorreu um erro.");
                }

            }
            return View(tipoRendimento);
        }

        // GET: TipoRendimentos/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoRendimentos/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var tipoRendimento = JsonConvert.DeserializeObject<TipoRendimento>(content);
                if (tipoRendimento == null) return HttpNotFound();
                return View(tipoRendimento);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);

        }

        // POST: TipoRendimentos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/TipoRendimentos/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }
    }
}
