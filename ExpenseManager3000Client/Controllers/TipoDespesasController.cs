﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using ExpenseManager3000Client.Models;
using ClienteMvcProdutos.Helpers;
using System.Net.Http;
using Newtonsoft.Json;

namespace ExpenseManager3000Client.Controllers
{
    public class TipoDespesasController : Controller
    {

        // GET: TipoDespesas
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoDespesas");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var tiposDespesa = JsonConvert.DeserializeObject<IEnumerable<TipoDespesa>>(content);
                return View(tiposDespesa);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: TipoDespesas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipoDespesas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "TipoDespesaId,Descricao,UserId")] TipoDespesa tipoDespesa)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string tiposDespesaJSON = JsonConvert.SerializeObject(tipoDespesa);
                HttpContent content = new StringContent(tiposDespesaJSON, System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/TipoDespesas", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }

        }

        // GET: TipoDespesas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoDespesas/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var tiposDespesa = JsonConvert.DeserializeObject<TipoDespesa>(content);
                if (tiposDespesa == null) return HttpNotFound();
                return View(tiposDespesa);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: TipoDespesas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "TipoDespesaId,Descricao,UserId")] TipoDespesa tipoDespesa)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string tiposDespesaJSON = JsonConvert.SerializeObject(tipoDespesa);
                HttpContent content = new StringContent(tiposDespesaJSON, System.Text.Encoding.Unicode, "application/json");
                var response = await client.PutAsync("api/TipoDespesas/" + tipoDespesa.TipoDespesaId, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }

        }

        // GET: TipoDespesas/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoDespesas/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var tiposDespesa = JsonConvert.DeserializeObject<TipoDespesa>(content);
                if (tiposDespesa == null) return HttpNotFound();
                return View(tiposDespesa);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);

        }

        // POST: TipoDespesas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/TipoDespesas/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }

        }
    }
}
