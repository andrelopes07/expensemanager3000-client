﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ExpenseManager3000Client.Startup))]
namespace ExpenseManager3000Client
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
