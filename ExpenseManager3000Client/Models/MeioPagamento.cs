﻿using System.ComponentModel;

namespace ExpenseManager3000Client.Models
{
    public class MeioPagamento
    {
        [DisplayName("Meio de Pagamento")]
        public int MeioPagamentoId { get; set; }
        [DisplayName("Meio de Pagamento")]
        public string Descricao { get; set; }
        public string UserId { get; set; }
    }
}