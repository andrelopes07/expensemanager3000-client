﻿using System.ComponentModel;

namespace ExpenseManager3000Client.Models
{
    public class TipoRendimento
    {
        [DisplayName("Tipo de Rendimento")]
        public int TipoRendimentoId { get; set; }
        [DisplayName("Tipo de Rendimento")]
        public string Descricao { get; set; }
        public string UserId { get; set; }
    }
}