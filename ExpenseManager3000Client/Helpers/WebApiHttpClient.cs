﻿using ExpenseManager3000Client.Helpers;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace ClienteMvcProdutos.Helpers
{
    public static class WebApiHttpClient
    {
        public const string WebApiBaseAddress = "http://localhost:10982/";
        public static HttpClient GetClient()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(WebApiBaseAddress);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
            new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            var session = HttpContext.Current.Session;
            if (session["token"] != null)
            {
                TokenResponse tokenResponse = (TokenResponse)session["token"];
                client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("bearer", tokenResponse.AccessToken);
            }
            
            return client;
        }

        public static void storeToken(TokenResponse token)
        {
            var session = HttpContext.Current.Session;
            session["token"] = token;
        }
    }
}